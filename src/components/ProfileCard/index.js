import React from "react";
import "./profileCard.css";

const ProfileCard = ({user}) => {
    return (
        <div className="profile-container">
            <p >Player: {user.username}</p>
            <div className="profile-text"></div>
            <p >Wins: {user.wins}</p>
            <div className="profile-text"></div>
            <p>Losses: {user.losses}</p>
            <div className="profile-text"></div>
            <p>Total Games: {user.totalGames}</p>
            <div className="profile-text"></div>
            <p>Score: {user.scores}</p>
        </div>
    )
};

export default ProfileCard;