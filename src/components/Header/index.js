import React from "react";
import "./header.css";
import { useNavigate } from "react-router-dom";
import { setResult } from "../../redux/resultSlice";
import { useDispatch } from "react-redux";

function Header() {
    const navigation = useNavigate();
    const dispatch = useDispatch();
    const handleLogout = () => {
        dispatch(setResult(false));
        navigation("/");
    };  
 return (
    <div className="header">
        <button className='title-button' onClick={() => navigation('/home')}>Tic Tac Toe</button>
        <button className='header-button' onClick={() => navigation('/game')}>Game</button>
        <button className='header-button' onClick={() => navigation('/leaderboard')}>LeaderBoard</button>
        <button className='header-button' onClick={handleLogout}>Logout</button>
    </div>
 );
}

export default Header;
