import React from "react";
import Square from "../Square";
import calculateWinner from "../CalculateWinner";
import { FaBomb } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { setResult, selectResult } from "../../redux/resultSlice";
import { setStatus } from "../../redux/statusSlice";
import { getFirestore, doc, updateDoc, increment } from "firebase/firestore";
import { selectPhoneNumber } from "../../redux/phoneSlice";
import { auth } from "../../firebase.config";

function OnlineBoard({ xIsNext, squares, onPlay, bomb }) {
    const result = useSelector(selectResult);
    const dispatch = useDispatch();
    const db = getFirestore();
    const phone = useSelector(selectPhoneNumber);
    const player = auth.currentUser;
   
    function handleClick(k) {
        if (calculateWinner(squares) || squares[k]) {
            return;
        }
        console.log(player.phoneNumber);
        function clearRowAndColumn(squares, k) {
            const row = Math.floor(k / 9);
            for (let i = 0; i < 9; i++) {
                squares[row * 9 + i] = null;
            }
        }

        const nextSquares = squares.slice();
      
        if (bomb) {
            clearRowAndColumn(nextSquares, k);
            nextSquares[k] = <FaBomb />;
            onPlay(nextSquares);

            setTimeout(() => {
                clearRowAndColumn(nextSquares, k);
                onPlay(nextSquares);
            }, 1000);
        } else if (xIsNext){
            nextSquares[k] = "X";
            
        } else {
            nextSquares[k] = "O";
        }
        onPlay(nextSquares);

    }

    const winner = calculateWinner(squares);

    const incrementWin = async () => {
        const userDocRef = doc(db, "users", phone);
        await updateDoc(userDocRef, {
            wins: increment(1),
            totalGames: increment(1),
            scores: increment(1),
            streak: increment(1),
        });
    };

    const incrementLoss = async () => {
        const userDocRef = doc(db, "users", phone);
        await updateDoc(userDocRef, {
            losses: increment(1),
            totalGames: increment(1),
            streak: 0,
        });
    };


    if (winner && result === false) {
        if (winner === "X") {
            incrementWin();
            dispatch(setStatus("You won!"));
            dispatch(setResult(true));
        } else {
            incrementLoss();
            dispatch(setStatus("You lost!"));
            dispatch(setResult(true));
        }
    }

    return (
        <>
            <div className="cube__face cube__face--front">
                <div className="board-row">
                    <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
                    <Square value={squares[1]} onSquareClick={() => handleClick(1)} />
                    <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
                </div>
                <div className="board-row">
                    <Square value={squares[3]} onSquareClick={() => handleClick(3)} />
                    <Square value={squares[4]} onSquareClick={() => handleClick(4)} />
                    <Square value={squares[5]} onSquareClick={() => handleClick(5)} />
                </div>
                <div className="board-row">
                    <Square value={squares[6]} onSquareClick={() => handleClick(6)} />
                    <Square value={squares[7]} onSquareClick={() => handleClick(7)} />
                    <Square value={squares[8]} onSquareClick={() => handleClick(8)} />
                </div>
            </div>
            <div className="cube__face cube__face--back">
                <div className="board-row">
                    <Square value={squares[9]} onSquareClick={() => handleClick(9)} />
                    <Square value={squares[10]} onSquareClick={() => handleClick(10)} />
                    <Square value={squares[11]} onSquareClick={() => handleClick(11)} />
                </div>
                <div className="board-row">
                    <Square value={squares[12]} onSquareClick={() => handleClick(12)} />
                    <Square value={squares[13]} onSquareClick={() => handleClick(13)} />
                    <Square value={squares[14]} onSquareClick={() => handleClick(14)} />
                </div>
                <div className="board-row">
                    <Square value={squares[15]} onSquareClick={() => handleClick(15)} />
                    <Square value={squares[16]} onSquareClick={() => handleClick(16)} />
                    <Square value={squares[17]} onSquareClick={() => handleClick(17)} />
                </div>
            </div>
            <div className="cube__face cube__face--left">
                <div className="board-row">
                    <Square value={squares[18]} onSquareClick={() => handleClick(18)} />
                    <Square value={squares[19]} onSquareClick={() => handleClick(19)} />
                    <Square value={squares[20]} onSquareClick={() => handleClick(20)} />
                </div>
                <div className="board-row">
                    <Square value={squares[21]} onSquareClick={() => handleClick(21)} />
                    <Square value={squares[22]} onSquareClick={() => handleClick(22)} />
                    <Square value={squares[23]} onSquareClick={() => handleClick(23)} />
                </div>
                <div className="board-row">
                    <Square value={squares[24]} onSquareClick={() => handleClick(24)} />
                    <Square value={squares[25]} onSquareClick={() => handleClick(25)} />
                    <Square value={squares[26]} onSquareClick={() => handleClick(26)} />
                </div>
            </div>
            <div className="cube__face cube__face--right">
                <div className="board-row">
                    <Square value={squares[27]} onSquareClick={() => handleClick(27)} />
                    <Square value={squares[28]} onSquareClick={() => handleClick(28)} />
                    <Square value={squares[29]} onSquareClick={() => handleClick(29)} />
                </div>
                <div className="board-row">
                    <Square value={squares[30]} onSquareClick={() => handleClick(30)} />
                    <Square value={squares[31]} onSquareClick={() => handleClick(31)} />
                    <Square value={squares[32]} onSquareClick={() => handleClick(32)} />
                </div>
                <div className="board-row">
                    <Square value={squares[33]} onSquareClick={() => handleClick(33)} />
                    <Square value={squares[34]} onSquareClick={() => handleClick(34)} />
                    <Square value={squares[35]} onSquareClick={() => handleClick(35)} />
                </div>
            </div>
            <div className="cube__face cube__face--top">
                <div className="board-row">
                    <Square value={squares[36]} onSquareClick={() => handleClick(36)} />
                    <Square value={squares[37]} onSquareClick={() => handleClick(37)} />
                    <Square value={squares[38]} onSquareClick={() => handleClick(38)} />
                </div>
                <div className="board-row">
                    <Square value={squares[39]} onSquareClick={() => handleClick(39)} />
                    <Square value={squares[40]} onSquareClick={() => handleClick(40)} />
                    <Square value={squares[41]} onSquareClick={() => handleClick(41)} />
                </div>
                <div className="board-row">
                    <Square value={squares[42]} onSquareClick={() => handleClick(42)} />
                    <Square value={squares[43]} onSquareClick={() => handleClick(43)} />
                    <Square value={squares[44]} onSquareClick={() => handleClick(44)} />
                </div>
            </div>
            <div className="cube__face cube__face--bottom">
                <div className="board-row">
                    <Square value={squares[45]} onSquareClick={() => handleClick(45)} />
                    <Square value={squares[46]} onSquareClick={() => handleClick(46)} />
                    <Square value={squares[47]} onSquareClick={() => handleClick(47)} />
                </div>
                <div className="board-row">
                    <Square value={squares[48]} onSquareClick={() => handleClick(48)} />
                    <Square value={squares[49]} onSquareClick={() => handleClick(49)} />
                    <Square value={squares[50]} onSquareClick={() => handleClick(50)} />
                </div>
                <div className="board-row">
                    <Square value={squares[51]} onSquareClick={() => handleClick(51)} />
                    <Square value={squares[52]} onSquareClick={() => handleClick(52)} />
                    <Square value={squares[53]} onSquareClick={() => handleClick(53)} />
                </div>
            </div>
        </>
    );
}

export default OnlineBoard;