import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyCk_wRaYmsRjMnz8_Z53v-e4A90sdF3U0c",
  authDomain: "tictactoe-task1-67e89.firebaseapp.com",
  projectId: "tictactoe-task1-67e89",
  storageBucket: "tictactoe-task1-67e89.appspot.com",
  messagingSenderId: "395988085908",
  appId: "1:395988085908:web:f6394fd7214098d1981881",
  databaseURL: "https://tictactoe-task1-67e89-default-rtdb.europe-west1.firebasedatabase.app/",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);