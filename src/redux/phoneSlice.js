import { createSlice } from '@reduxjs/toolkit';

export const phoneSlice = createSlice({
    name: 'phone',
    initialState: {
        phone: null,
    },
    reducers: {
        setPhoneNumber: (state, action) => {
            state.phone = action.payload;
        },
    },
});

export const { setPhoneNumber } = phoneSlice.actions;

export const selectPhoneNumber = (state) => state.phone.phone;

export default phoneSlice.reducer;