import { configureStore } from '@reduxjs/toolkit'
import resultReducer from './resultSlice'
import statusReducer from './statusSlice'
import phoneReducer from './phoneSlice'
import timerReducer from './timerSlice'

export default configureStore({
  reducer: {
    result: resultReducer,
    status: statusReducer,
    phone: phoneReducer,
    timer: timerReducer,
  },
})
