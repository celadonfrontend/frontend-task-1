import { createSlice } from "@reduxjs/toolkit";

export const timerSlice = createSlice({
    name: "timer",
    initialState: {
        countdown:  30,
        gameActive: true,
    },
    reducers: {
        startCountdown: (state) => {
          state.gameActive = false;
          state.countdown = 30;
        },
        stopCountdown: (state) => {
          state.gameActive = true;
          state.countdown = 0;
        },
        updateCountdown: (state) => {
          if (state.countdown > 0) {
            state.countdown -= 1;
          }
        },
     },
});

export const { startCountdown, stopCountdown, updateCountdown } = timerSlice.actions;

export const selectGameActive = (state) => state.timer.gameActive;

export default timerSlice.reducer;