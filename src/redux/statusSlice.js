import { createSlice } from "@reduxjs/toolkit";

export const statusSlice = createSlice({
    name: "status",
    initialState: {
        status:  "Welcome",
    },
    reducers: {
        setStatus: (state, action) => {
            state.status = action.payload;
        },
    },
});

export const { setStatus } = statusSlice.actions;

export const selectStatus = (state) => state.status.status;

export default statusSlice.reducer;