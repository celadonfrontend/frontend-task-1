import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import LeaderBoard from './pages/LeaderBoard';
import Game from './pages/Game';
import { Provider } from 'react-redux';
import store from './redux/store';
import Room from './pages/Room';

function App() {
    return (
        <>
            <Provider store={store}>
                <Router>
                    <Routes>
                        <Route exact path="/" element={<Login />} />
                        <Route path="/home" element={<Home  />} />
                        <Route path="/room" element={<Room />} />
                        <Route path="/room/:id" element={<Room />} />
                        <Route path="/game" element={<Game />} />
                        <Route path="/leaderboard" element={<LeaderBoard />} />
                        <Route path="/register" element={<Register />} />     
                    </Routes>
                </Router>
            </Provider>
        </>
    );
}

export default App;
