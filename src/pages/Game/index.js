import React, { useState, useEffect } from 'react';
import ProfileCard from '../../components/ProfileCard';
import Board from '../../components/Board';
import { FaBomb } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import { setResult } from '../../redux/resultSlice';
import { selectStatus, setStatus } from '../../redux/statusSlice';
import { doc, getFirestore, onSnapshot } from "firebase/firestore";
import { selectPhoneNumber } from '../../redux/phoneSlice';
import { useNavigate } from 'react-router-dom';
import Header from '../../components/Header';

function Game() {
  const [history, setHistory] = useState([Array(54).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const xIsNext = currentMove % 2 === 0;
  const currentSquares = history[currentMove];
  const [bomb, setBomb] = useState(false);
  const [userData, setUserData] = useState("");
  const dispatch = useDispatch();
  const db = getFirestore();
  const phone = useSelector(selectPhoneNumber);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchUserData = () => {
      if (!phone || phone === null) {
        console.log("No user ID available");
        navigate('/');
        return;
      }
  
      const userDocRef = doc(db, "users", phone);
      const unsubscribe = onSnapshot(userDocRef, (docSnap) => {
        if (docSnap.exists()) {
          setUserData(docSnap.data());
        } else {
          console.log("No such document!");
        }
      });
  
      return unsubscribe;
   };
  
   const unsubscribe = fetchUserData();
   return () => {
      unsubscribe && unsubscribe();
   };
  }, [phone]);

   console.log(userData);
  
  function rotateCube(e) {
    const container = document.querySelector('.game-board');

    if (container) {
      const dx = e.clientX / window.innerWidth;
      const dy = e.clientY / window.innerHeight;

      container.style.transform = `rotateX(${dy * 300}deg) rotateY(${dx * 300}deg)`;
    }
  }

  window.addEventListener('mousemove', rotateCube);
  const status = useSelector(selectStatus);

  function handlePlay(nextSquares) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);

    console.log("Current move: ", currentMove);
    console.log("History: ", history);
    console.log("next squares: ", nextSquares);
  }

  function jumpTo(nextMove) {
    setBomb(false);
    dispatch(setResult(false));
    dispatch(setStatus("Game started"));
    setCurrentMove(nextMove);

  }

  const moves = history.map((squares, move) => {
    let description;
    if (move > 0) {
      description = "Go to move #" + move;
    } else {
      description = "Go to game start";
    }
    return (
      <div key={move}>
        <button className='description-button' onClick={() => jumpTo(move)}>{description}</button>
      </div>
    );
  });

  return (
    <>
    <Header />
    <div className="game">
     
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
        <div >
          <ProfileCard user={userData} />
        </div>
        <div className="status">
          <span>{status}</span>
        </div>
        <div className="cube-container">
          <div className="game-board">
            <Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} bomb={bomb} />
          </div>
        </div>
      </div>
      <div className="game-info">
        <span>{moves}</span>
      </div>
      {bomb ? <button className="bomb-button" onClick={() => setBomb(!bomb)}>Bomb is active</button>
        : <button className="bomb-button" onClick={() => setBomb(!bomb)}>Place Bomb <FaBomb /></button>}
    </div>
    </>
  );
}


export default Game;