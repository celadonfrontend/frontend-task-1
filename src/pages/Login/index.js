import React, { useState } from 'react'
import './login.css';
import { useNavigate } from "react-router-dom";
import { RecaptchaVerifier, signInWithPhoneNumber, getAdditionalUserInfo } from 'firebase/auth';
import { auth } from '../../firebase.config';
import PhoneInput from 'react-phone-number-input'
import OtpInput from 'react-otp-input';
import { setPhoneNumber } from '../../redux/phoneSlice';
import { useDispatch } from 'react-redux';
import { Blocks } from 'react-loader-spinner';

const Login = () => {
    const [phone, setPhone] = useState('');
    const [flag, setFlag] = useState(false);
    const [otp, setOtp] = useState('');
    const [confirmationResult, setConfirmationResult] = useState(null);
    const navigation = useNavigate();
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);

    const sendOtp = async () => {
        setLoading(true);
        try {
            if (!phone) return alert('Please enter phone number');
            const recaptcha = new RecaptchaVerifier(auth, "recaptcha-container", { size: "invisible" });
            recaptcha.render();
            const confirmation = await signInWithPhoneNumber(auth, phone, recaptcha);
            setConfirmationResult(confirmation);
            setFlag(true);
            console.log(confirmation);
            console.log(phone)
        } catch (err) {
            console.log(err);
        } finally {
            setLoading(false);
        }
    };

    const verifyOtp = async (e) => {
        e.preventDefault();
        console.log(confirmationResult, "confirmationResult");
        setLoading(true);
        if (!otp) return alert('Please enter OTP');
        try {
            const result = await confirmationResult.confirm(otp);
            console.log(result);
            const { isNewUser } = getAdditionalUserInfo(result);
            
            dispatch(setPhoneNumber(phone));
            { isNewUser ? navigation('/register', { state: phone }) : navigation('/home', { state: phone }) };
        } catch (err) {
            console.log(err);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className='login-page'>
            <h1 className='title'>Tic Tac Toe</h1>
            <div id="recaptcha-container"></div>
            <div className='input-container'>
                {!flag ? <div><PhoneInput
                    className='input'
                    international
                    placeholder="Enter phone number"
                    value={phone}
                    onChange={setPhone}
                    defaultCountry='TR' />
                    <button className='button' onClick={sendOtp}>Send OTP</button></div> : null}
                {flag ?
                    <div>
                        <OtpInput
                            value={otp}
                            onChange={setOtp}
                            numInputs={6}
                            renderSeparator={<span>-</span>}
                            renderInput={(props) => <input {...props} />}
                            inputStyle={{ width: '27px', height: '27px', borderRadius: '8px' }}
                        />
                        <button className='button' onClick={verifyOtp}>Verify OTP</button>
                    </div>
                    : null
                }
                <div className='blocks-container'>
                    {loading && <Blocks height="80"
                        width="80"
                        radius="9"
                        color="green"
                        ariaLabel="loading"
                    />
                    }
                </div>
            </div>
        </div>
    )
};

export default Login;