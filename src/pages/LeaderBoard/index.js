import React, {useState, useEffect} from 'react'
import Header from '../../components/Header';
import './leaderboard.css';
import { getFirestore, collection, onSnapshot } from "firebase/firestore";
import { IconContext } from "react-icons";
import { ImFire } from "react-icons/im";
import { auth } from '../../firebase.config';
import { useNavigate } from 'react-router-dom';

const LeaderBoard = () => {
    const [userData, setUserData] = useState("");
    const db = getFirestore();
    const user = auth.currentUser;
    const navigate = useNavigate();
    useEffect(() => {
        const fetchUserData = () => {
            console.log(user);
            if(!user) {
                navigate('/');
            }
           const userCollRef = collection(db, "users");
           const unsubscribe = onSnapshot(userCollRef, (querySnapshot) => {
             const userDataArray = [];
             querySnapshot.forEach((doc) => {
               userDataArray.push({ ...doc.data(), id: doc.id });
             });
             setUserData(userDataArray);
           });
       
           return unsubscribe;
        };
       
        const unsubscribe = fetchUserData();
        return () => {
           unsubscribe && unsubscribe();
        };
       }, []);
    return (
        <div >
            <Header />
            <div className='leaderboard-container'>
                <h1 className='leaderboard-title'>LeaderBoard</h1>
                <div >
                    <table className='leaderboard-table'>
                        <thead>
                            <tr>
                                <th>Players</th>
                                <th>Wins</th>
                                <th>Defeats</th>
                                <th>Total Games</th>
                                <th>Scores</th>
                                <th>Win Rate</th>
                                <th>Streak</th>
                            </tr>
                        </thead>
                        <tbody>
                            {userData && userData.map((userData, index) => (
                                <tr key={index} >
                                    <td className='text'>{userData.username}</td>
                                    <td className='text'>{userData.wins}</td>
                                    <td className='text'>{userData.losses}</td>
                                    <td className='text'>{userData.totalGames}</td>
                                    <td className='text'>{userData.scores}</td>
                                    <td>{(userData.wins / userData.totalGames * 100).toFixed(2)}%</td>
                                    <td className='text'>
                                        {userData.streak}{' '}
                                        {userData.streak >=  5 && userData.streak <=  10 && <ImFire />}{' '}
                                        {userData.streak >=  10 &&  userData.streak <=  20 &&
                                        <IconContext.Provider value={{ color: "yellow"}}>
                                            <ImFire />
                                        </IconContext.Provider>}
                                        {userData.streak >  20 &&  
                                        <IconContext.Provider value={{ color: "red"}}>
                                            <ImFire />
                                        </IconContext.Provider>}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
};

export default LeaderBoard;
