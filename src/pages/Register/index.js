import React, { useState } from 'react'
import './register.css';
import { getFirestore, doc, setDoc } from "firebase/firestore";
import { useLocation, useNavigate } from 'react-router-dom';

const Register = () => {
    const [username, setUsername] = useState('');
    const location = useLocation();
    const navigation = useNavigate();
    const db = getFirestore();
    const phone = location.state;

    const addUser = async () => {
        try {
            const docRef = doc(db, "users", phone);
            await setDoc(docRef, {
                username: username,
                wins: 0,
                losses: 0,
                totalGames: 0,
                winRate: 0,
                scores: 0,
                streak: 0,
            });
            navigation('/home');
            console.log("Document written with ID: ", docRef.id);
        } catch (e) {
            console.error("Error adding document: ", e);
        }
    };

    return (
        <div className='register-page'>
            <div className='input-container'>
                <input className='input' type="text" placeholder="Enter your name" onChange={(event) => setUsername(event.target.value)} />
                <button className='button' onClick={addUser}>Register</button>
            </div>
        </div>
    )
}

export default Register;