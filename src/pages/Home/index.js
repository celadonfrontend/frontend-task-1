import React, { useState } from "react";
import "./home.css";
import Header from "../../components/Header";
import { useNavigate } from "react-router-dom";
import { getFirestore, doc, setDoc, updateDoc } from "firebase/firestore";
import { auth } from "../../firebase.config";
import { Alert } from "@mui/material";

export default function Home() {
  const [flag, setFlag] = useState(false);
  const [joinRoomFlag, setJoinRoomFlag] = useState(false);
  const [joinRoomId, setJoinRoomId] = useState(false);
  const navigate = useNavigate();
  const db = getFirestore();
  const player = auth.currentUser;

  const createRoom = async () => {
    const roomId = Math.random().toString(36).substring(7);
    const docRef = doc(db, "rooms", roomId);
    try {
      await setDoc(docRef, {
        status: "Waiting for opponent",
        roomId: roomId,
        player1: player.phoneNumber,
        player2: null,
        squares: [null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null],
        currentMove:  0,
        bomb: false,
        xIsNext: true,
        playerTurn: player.phoneNumber,
        isGameEnd: false,
      });
      navigate(`/room/${roomId}`, { state: { roomId: roomId } });
    } catch (err) {
      console.log(err);
    }
  };

  const joinRoom = async () => {
    try {
      await updateDoc(doc(db, "rooms", joinRoomId), {
        status: "Good Luck!",
        player2: player.phoneNumber,
      });
      navigate(`/room/${joinRoomId}`, { state: { roomId: joinRoomId } });
    }
    catch (err) {
      console.log(err);
    }
    
  };
  
  const handleBack = () => {
    setFlag(false);
    setJoinRoomFlag(false);
  };

  return (
    <div>
    <Header />
    <div className="home">
      <h1 className="home-title">Choose a game mode</h1>
      {!flag ? (
        <>
          <button className="home-button" onClick={() => navigate('/game')}>Play with computer</button>
          <button className="home-button" onClick={() => setFlag(true)}>Play with your friend</button>
        </>
      ) : (
        <>
          <button className="home-button" onClick={createRoom}>Create a room</button>
          <button className="home-button" onClick={() => setJoinRoomFlag(true)}>Join room</button>
          <button className="home-button" onClick={handleBack}>Back</button>
          {joinRoomFlag && (
            <>
              <input
                className="home-button-input"
                type="text"
                placeholder="Enter room ID"
                onChange={(e) => setJoinRoomId(e.target.value)}
              />
              <button className="home-button" onClick={joinRoom}>Join</button>
            </>           
          )}
        </>
      )}
    </div>
  </div>
  );
}