import React, { useState, useEffect } from 'react';
import ProfileCard from '../../components/ProfileCard';
import Board from '../../components/Board';
import OnlineBoard from '../../components/OnlineBoard';
import { FaBomb } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import { setResult } from '../../redux/resultSlice';
import { selectStatus, setStatus } from '../../redux/statusSlice';
import { doc, getFirestore, onSnapshot, updateDoc } from "firebase/firestore";
import { selectPhoneNumber } from '../../redux/phoneSlice';
import { useNavigate } from 'react-router-dom';
import Header from '../../components/Header';
import { useParams } from 'react-router-dom';
import { auth } from '../../firebase.config';
import { getDatabase, ref, onValue, set, onDisconnect, off } from "firebase/database";
import { startCountdown, stopCountdown, updateCountdown } from '../../redux/timerSlice';
import { selectGameActive } from '../../redux/timerSlice';

function OnlineGame() {
  const [history, setHistory] = useState([Array(54).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const [xIsNext, setXIsNext] = useState(true);
  const currentSquares = history[currentMove];
  const [bomb, setBomb] = useState(false);
  const [userData, setUserData] = useState("");
  const dispatch = useDispatch();
  const db = getFirestore();
  const phone = useSelector(selectPhoneNumber);
  const navigate = useNavigate();
  const [squares, setSquares] = useState([Array(54).fill(null)]);
  const player = auth.currentUser.phoneNumber;
  const roomId = useParams().id;
  const [playerTurn, setPlayerTurn] = useState("");
  const [playerData, setPlayerData] = useState([]);
  const [status, setStatus] = useState("");
  const [playerStatus, setPlayerStatus] = useState();
  const [timer, setTimer] = useState(15);
  const rdb = getDatabase();
  const userStatusRef = ref(rdb, '/status/' + player);


  useEffect(() => {
    set(userStatusRef, { state: 'online', last_changed: Date.now() });

    onDisconnect(userStatusRef).set({ state: 'offline', last_changed: Date.now() });

    const statusListener = onValue(userStatusRef, (snapshot) => {
      const status = snapshot.val();
      setPlayerStatus(status);
      if (status.state === 'offline') {
        dispatch(startCountdown());
      } else if (status.state === 'online') {
        dispatch(stopCountdown());
      }
    });

    return () => off(statusListener);
  }, [player, dispatch]);

  useEffect(() => {
    if (selectGameActive) return;

    const timer = setInterval(() => {
      dispatch(updateCountdown());
    }, 1000);

    return () => clearInterval(timer);
 }, [selectGameActive, dispatch]);

  useEffect(() => {
    console.log("Room ID: ", roomId);
    const gameRoomRef = doc(db, "rooms", roomId);
    const unsubscribe = onSnapshot(gameRoomRef, (docSnapshot) => {
      if (docSnapshot.exists()) {
        const gameData = docSnapshot.data();
        setPlayerData(gameData);
        setHistory(gameData.history || [Array(54).fill(null)]);
        setCurrentMove(gameData.currentMove ||  0);
        setBomb(gameData.bomb || false);
        setUserData(gameData.userData || "");
        setSquares(gameData.squares || [Array(54).fill(null)]);
        setXIsNext(gameData.xIsNext || false);
        setPlayerTurn(gameData.playerTurn || "");
        setStatus(gameData.status || "");
        console.log("Current data: ", gameData.userData);
      } else {
        console.log("No such document!");
      }
    });

    return () => unsubscribe();
  }, []);

  useEffect(() => {
    const fetchUserData = () => {
      if (!player || player === null) {
        console.log("No user ID available");
        navigate('/');
        return;
      }
  
      const userDocRef = doc(db, "users", player);
      const unsubscribe = onSnapshot(userDocRef, (docSnap) => {
        if (docSnap.exists()) {
          setUserData(docSnap.data());

        } else {
          console.log("No such document!");
        }
      });
  
      return unsubscribe;
   };
  
   const unsubscribe = fetchUserData();
   return () => {
      unsubscribe && unsubscribe();
   };
  }, [playerData] );

  function rotateCube(e) {
    const container = document.querySelector('.game-board');

    if (container) {
      const dx = e.clientX / window.innerWidth;
      const dy = e.clientY / window.innerHeight;

      container.style.transform = `rotateX(${dy * 300}deg) rotateY(${dx * 300}deg)`;
    }
  }

  window.addEventListener('mousemove', rotateCube);

  async function handlePlay(nextSquares) {
    console.log(playerStatus, "status");

    if(status === "Waiting for opponent") {
      console.log("Oyuncu bekleniyor!");
      return;
    }

    if(playerTurn !== player) {
      console.log("Sıra sende değil!");
      console.log(playerTurn, "playerTurn");
      console.log(player, "player");
      return;
    }

    const gameRoomRef = doc(db, "rooms", roomId);
    try {
      if(player)
        await updateDoc(gameRoomRef, {
            squares: nextSquares,
            currentMove: currentMove + 1,
            xIsNext: !xIsNext,
            bomb: bomb,
            playerTurn: xIsNext ? playerData.player1 : playerData.player2,

        });
    } catch (error) {
        console.error("Oyun durumu güncellenirken bir hata oluştu:", error);
    }
}

  const resetGame = async () => {
    const gameRoomRef = doc(db, "rooms", roomId);
    try {
      await updateDoc(gameRoomRef, {
        squares: [null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null,null,null,null,null,null,
          null,null,null,null,null],
        currentMove:  0,
        bomb: false,
        xIsNext: true,
      });
      setSquares([null,null,null,null,null,null,null,null,null,null,
        null,null,null,null,null,null,null,null,null,null,
        null,null,null,null,null,null,null,null,null,null,
        null,null,null,null,null,null,null,null,null,null,
        null,null,null,null,null,null,null,null,null,null,
        null,null,null,null,null]);
      setHistory([Array(54).fill(null)]);
      setCurrentMove(0);
      setBomb(false);
      setUserData("");
    } catch (error) {
      console.error("Error resetting the game:", error);
    }
  };
  
  return (
    <>
    <Header />
    <div className="game">
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
        <div >
          <ProfileCard user={userData} />
        </div>
        <div className="status">
          <span>{status}</span>
        </div>
        <div className="cube-container">
          <div className="game-board">
            <OnlineBoard xIsNext={xIsNext} squares={squares} onPlay={handlePlay} bomb={bomb} />
          </div>
        </div>
      </div>
     <div>
     <div style={{padding: 46}}>
        <button onClick={resetGame}>Restart Game</button>
      </div>
     </div>
      {bomb ? <button className="bomb-button" onClick={() => setBomb(!bomb)}>Bomb is active</button>
        : <button className="bomb-button" onClick={() => setBomb(!bomb)}>Place Bomb <FaBomb /></button>}
    </div>
    </>
  );
}

export default OnlineGame;